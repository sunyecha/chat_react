# Chat_React
## Sujet : Application de chat pour utilisateurs

## Pile technologique :

 1. Frontend : React
 2. Backend : Springboot
 3. Technologie de communication : WebSocket
 4. Rendu : Bootstrap et autres bibliothèques de composants

## Description de la structure :

 1. Sur le côté gauche de l'interface entière sont affichés "Planifier une discussion", "Mes salons de discussion", "Mes invitations" ainsi que tous les canaux (les canaux commençant par \# sont les canaux propres de l'utilisateur, ceux commençant par \#\# sont les canaux où l'utilisateur est invité). En utilisant les hooks de router pour le déploiement sur les composants, nous permettons l'affichage de l'interface correspondante sur le côté droit en cliquant sur les boutons de gauche.

 2. Afin de distinguer l'interface de chaque utilisateur, le nom de l'utilisateur est affiché dans le coin supérieur gauche après son entrée sur la page de chat. À côté du nom de l'utilisateur, un bouton de déconnexion est placé, qui, lorsqu'il est cliqué, efface les informations de session actuelles de l'utilisateur et le renvoie à l'interface de connexion.

 3. Dans l'interface de chat de l'utilisateur, cliquer sur le bouton Utilisateur permet d'inviter des utilisateurs et d'afficher le statut en ligne des utilisateurs actuels du channel.

 4. Après être entré dans l'interface de chat, il est possible d'envoyer et de recevoir des messages qui seront affichés sur le côté droit de l'interface.

## Gestion de l'état :

 1. Pour la gestion de l'état global, nous utilisons Redux pour une gestion unifiée.
 2. Pour la gestion de l'état local, nous utilisons useState.
 3. En outre, nous utilisons useEffect, useMemo, useReducer, et d'autres Hooks pour la gestion du rendu, etc.

## Types de channels :
Les channels commençant par # sont celles que vous avez créées.
Les channels commençant par ## sont celles où vous avez été invité.

## Nous avons placé toutes les données de test dans data.sql.

## Sécurité :
 Nous utilisons JWT pour authentifier chaque session, chaque session dispose d'un token aléatoire pour interagir avec les données backend.

## Compléments des tâches non achevées dans la présentation :
 1. Gestion de la durée de vie de chaque canal : achevée, dans la version actuelle, si le temps défini par le propriétaire est atteint, la fenêtre de chat correspondante sera supprimée de la liste des canaux（Pour les tests, l'unité de durée est la seconde.）
 2. Connexion WebSocket: nous avons reussi a implementer une communication websocket via le protocole STOMP.
 (Durant les échanges dans la salle, les messages en bleu sont ceux que vous envoyez, tandis que les messages en blanc sont ceux reçus des autres.)

Note:Le seul point un peu instable est que la liste des channels ne se met pas à jour immédiatement à 100 % après qu'un utilisateur reçoit une invitation à un autre channel, il a besoin parfois de créer un nouveau channel pour aider à rafraîchir la liste des channels.
