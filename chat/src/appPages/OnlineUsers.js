import React from 'react';
import { ListGroup } from 'react-bootstrap';

function OnlineUsers() {
  return (
    <div>
      <h5>Online Users</h5>
      <ListGroup>
        <ListGroup.Item>User1</ListGroup.Item>
        <ListGroup.Item>User2</ListGroup.Item>
        <ListGroup.Item>User3</ListGroup.Item>
      </ListGroup>
    </div>
  );
}

export default OnlineUsers;

