import React, {useState, useRef} from 'react';
import {useDispatch} from 'react-redux';
import {addMessage} from '../redux/reducer/messageSlice';
import {increment} from '../redux/reducer/checkNewRoomSlice'
import UserSession from "../UserSession";
import {increment2} from '../redux/reducer/checkDeleteRoomSlice';
import {useNavigate} from 'react-router-dom';

function CreateChannel() {
    const navigate = useNavigate();

    //get the current time
    function getCurrentDate() {
        const now = new Date();
        const year = now.getFullYear();
        const month = now.getMonth() + 1;
        const day = now.getDate();

        return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
    }

    //get user id
    UserSession.getUserId();
    //all the data
    const [channelName, setChannelName] = useState('');
    const [description, setDescription] = useState('');
    const [date, setDate] = useState(getCurrentDate());
    const [duration, setDuration] = useState(1);

    const timersRef = useRef({});

    // const [selectedUserId, setSelectedUserId] = useState('');
    const dispatch = useDispatch();

    //send channel creation message
    const sendData = async () => {
        const url = '/api/channels?username=' + UserSession.getUsername();
        const payload = {
            title: channelName,
            description: description,
            date: date,
            duration: duration,
            ownerId: parseInt(UserSession.getUserId(), 10),
        };

        console.log(JSON.stringify(payload));

        try {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + UserSession.getToken()
                },
                body: JSON.stringify(payload)
            });
            console.log(response);

            if (response.ok) {
                const data = await response.json();
                timersRef.current[data.id] = setTimeout(() => {
                    deleteData(data.id);
                    dispatch(increment2());
                    navigate('*');
                    dispatch(increment2());
                    delete timersRef.current[data.id];
                }, duration * 1000);
                console.log('Success:', data);
            } else {
                throw new Error('Something went wrong');
            }
            if (response.status === 201) {
                dispatch(addMessage({chatId: channelName, message: ''}));
                dispatch(increment());
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (channelName.trim() !== '') {
            sendData();
            setChannelName('');
            setDescription('');
            setDuration(1);
        }
    };

    const deleteData = (idChannel) => {
        fetch('/api/channels/' + idChannel + '?username=' + UserSession.getUsername(), {
            method: 'DELETE',
            headers: {
                'accept': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };

    return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
            <form onSubmit={handleSubmit} style={{
                width: '100%',
                maxWidth: '400px',
                padding: '20px',
                backgroundColor: 'white',
                borderRadius: '5px',
                boxShadow: '0 2px 10px rgba(0,0,0,0.1)'
            }}>
                <h2 style={{textAlign: 'center', marginBottom: '20px'}}>Create Chat Channel</h2>

                <div style={{marginBottom: '20px'}}>
                    <label style={{display: 'block', marginBottom: '5px'}}>Channel Name:</label>
                    <input
                        type="text"
                        name="channelName"
                        id="channelName"
                        placeholder="Enter channel name"
                        value={channelName}
                        onChange={e => setChannelName(e.target.value)}
                        style={{width: '95%', padding: '10px', borderRadius: '5px', border: '1px solid #ccc'}}
                    />
                </div>

                <div style={{marginBottom: '20px'}}>
                    <label style={{display: 'block', marginBottom: '5px'}}>Description:</label>
                    <input
                        type="text"
                        name="description"
                        id="description"
                        placeholder="Enter description"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        style={{width: '95%', padding: '10px', borderRadius: '5px', border: '1px solid #ccc'}}
                    />
                </div>

                <div style={{marginBottom: '20px'}}>
                    <label style={{display: 'block', marginBottom: '5px'}}>Duration:</label>
                    <input
                        type="number"
                        name="duration"
                        id="duration"
                        placeholder="Enter a duration"
                        value={duration}
                        onChange={e => setDuration(parseInt(e.target.value) || '')}
                        min="1"
                        style={{width: '95%', padding: '10px', borderRadius: '5px', border: '1px solid #ccc'}}
                    />
                </div>

                <button type="submit" style={{
                    width: '100%',
                    padding: '10px',
                    backgroundColor: '#007bff',
                    color: 'white',
                    border: 'none',
                    borderRadius: '5px',
                    cursor: 'pointer'
                }}>
                    Create Channel
                </button>
            </form>
        </div>
    );
}

export default CreateChannel;