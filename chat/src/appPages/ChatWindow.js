import React from 'react';
import { useState, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { incrementNum } from '../redux/reducer/checkNewUserAddedSlice';
import Modal from '../components/Modal';
import './css/ChatWindow.css';
import UserSession from "../UserSession";
import { useStomp } from "../websocket/StompHandler";

function ChatWindow() {
   const { messages, sendMessage } = useStomp();
   const dispatch = useDispatch();

   const incrementAdd = useSelector(state => state.checkNewUserAdded.value);

   //modal
   const [isModalOpen, setIsModalOpen] = useState(false);

   const openModal = () => setIsModalOpen(true);
   const closeModal = () => setIsModalOpen(false);

   //message
   const [message, setMessage] = useState('');

   //get all params
   const { channelId, channelName } = useParams();

   //get all invited users
   const [usersInvited, setUsersInvited] = useState([]);

   const handleSendMessage = (event) => {
      console.log('Message to send:', message);
      event.preventDefault();
      sendMessage(channelId, message);
      setMessage('');
   };

   //sort
   const sortedUsers = useMemo(() => {
      return [...usersInvited].sort((a, b) => a.id - b.id);
   }, [usersInvited]);

   //get all invited users
   useEffect(() => {
      fetch('/api/channels/' + channelId + '/invited_users?username=' + UserSession.getUsername(), {
         method: 'GET',
         headers: {
            'accept': 'application/json',
            'Authorization': 'Bearer ' + UserSession.getToken(),
         }
      })
         .then(response => {
            if (!response.ok) {
               throw new Error('Network response was not ok');
            } else {
               console.log('ok');
            }
            return response.json();
         })
         .then(data => {
            // setChannelsProp(data);
            // dispatch(allInvitedChannels(data));
            setUsersInvited(data);
         })
         .catch(error => {
            console.error('Error:', error);
         });
   }, [incrementAdd]);

   //Model
   const [selectedUserId, setSelectedUserId] = useState(0);
   const allUsers = useSelector(state => state.allUsers.list);

   const handleSelectChange = (event) => {
      setSelectedUserId(event.target.value);
   };

   const handleButtonClick = () => {
      fetch('/api/channels/' + channelId + '/add_user/' + selectedUserId + '?username=' + UserSession.getUsername(), {
         method: 'PUT',
         headers: {
            'accept': 'application/json',
            'Authorization': 'Bearer ' + UserSession.getToken(),
         }
      })
         .then(response => {
            if (!response.ok) {
               throw new Error('Network response was not ok');
            } else {
               console.log('ok');
               dispatch(incrementNum());
               setSelectedUserId('');
            }
            return response.json();
         })
         .then(data => {
            // setUsersInvited(data);
            console.log('add well:', data);
         })
         .catch(error => {
            console.error('Error:', error);
         });
   };

   return (
      <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
         <div style={{
            background: '#4F4F4F',
            color: 'white',
            padding: '20px',
            fontSize: '24px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
         }}>
            Chat Room : {channelName}
            <button onClick={openModal} style={{ fontSize: '16px', padding: '10px 20px' }}>Users</button>
         </div>
         <Modal isOpen={isModalOpen} onClose={closeModal}>
            <div style={{
               width: '300px',
               height: '400px',
               display: 'flex',
               flexDirection: 'column',
               overflow: 'auto'
            }}>
               <div style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: '0px'
               }}>
                  <h3>Users</h3>
                  <div style={{ display: 'flex', gap: '8px' }}>
                     <select value={selectedUserId} onChange={handleSelectChange}>
                        <option value="" disabled selected>
                           Please select a user
                        </option>
                        {allUsers.map((user) => (
                           <option key={user.id} value={user.id}>
                              {user.firstName} {user.lastName}
                           </option>
                        ))}
                     </select>
                     <button onClick={handleButtonClick}>Add</button>
                  </div>
               </div>
               <hr style={{ height: '3px', backgroundColor: '#ccc', border: 'none', margin: '0px 0' }} />

               <table style={{ width: '100%', textAlign: 'left', marginTop: '5px' }}>
                  <thead>
                     <tr>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     {sortedUsers.map((user, index) => (
                        <tr key={index}>
                           <td>{user.firstName}</td>
                           <td>{user.lastName}</td>
                           <td><span style={{ color: 'green' }}>Online</span></td>
                        </tr>
                     ))}
                  </tbody>
               </table>
            </div>
         </Modal>

         {/* content */}
         <div className="message-container">
            {messages[channelId]?.map((message, index) => {
               console.log("HEYHEY: ", messages)
               const isIncoming = message.sender !== UserSession.getUserId();
               return (
                  <div key={index} className={`${isIncoming ? "message-bubble left" : "message-bubble right"}`}>
                     {message.content}
                  </div>
               )
            }
            )}
         </div>

         {/* input */}
         <form style={{ display: 'flex', padding: '10px', borderTop: '1px solid #ccc' }} onSubmit={handleSendMessage}>
            <input
               type="text"
               value={message}
               onChange={e => setMessage(e.target.value)}
               style={{ flex: 1, marginRight: '10px', padding: '10px' }}
               placeholder="Enter message"
            />
            <button type="submit" style={{ padding: '10px 20px' }}>Send</button>
         </form>
      </div>
   );
}

export default ChatWindow;

