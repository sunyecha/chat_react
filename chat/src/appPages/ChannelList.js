import React, {useState, useEffect} from 'react';
import {Card, ListGroup} from 'react-bootstrap';
import './css/ChannelList.css';
import ChannelHeader from '../components/ChannelHeader';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';
import UserSession from "../UserSession";

function ChannelList() {
    const newRoom = useSelector(state => state.checkNewRoom.value);
    const deleteRoom = useSelector(state => state.checkDeleteRoom.value);
    const [myChannels, setMyChannels] = useState([]);
    const [myInvitedChannels, setMyInvitedChannels] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [reloadAllowed, setReloadAllowed] = useState(true);

    useEffect(() => {
        fetchMyChannels();
        fetchInviteChannels();
        setLoading(false);
    }, [newRoom, deleteRoom]);


    const fetchMyChannels = async () => {
        await fetch('/api/users/' + UserSession.getUserId() + '/owner_channels?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
                setMyChannels(data);
                console.log('Success:', data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };

    const fetchInviteChannels = async () => {
        await fetch('/api/users/' + UserSession.getUserId() + '/invite_channels?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
                // setChannelsProp(data);
                setMyInvitedChannels(data);
                console.log('Successful:', data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    if (isLoading) {
        return <div>
            <div>
                <div className="channel-header">
                    <ChannelHeader/>
                </div>

                <ListGroup>
                    <ListGroup.Item>
                        <Card className="text-center mb-2 shadow-sm">
                            <Card.Body className="custom-card-body"><Link to="/create-chat">Planning a
                                discussion</Link></Card.Body>
                        </Card>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Card className="text-center">
                            <Card.Body className="custom-card-body"><Link to="/chat-rooms">My chat
                                rooms</Link></Card.Body>
                        </Card>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Card className="text-center">
                            <Card.Body className="custom-card-body"><Link to="/chat-invitations">My
                                invitations</Link></Card.Body>
                        </Card>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Card className="text-center">
                            <Card.Body className="custom-card-body">Loading channels...</Card.Body>
                        </Card>
                    </ListGroup.Item>
                </ListGroup>
            </div>
        </div>
    }

    return (
        <div>
            <div className="channel-header">
                <ChannelHeader/>
            </div>

            <ListGroup>
                <ListGroup.Item>
                    <Card className="text-center mb-2 shadow-sm">
                        <Card.Body className="custom-card-body"><Link to="/create-chat">Planning a
                            discussion</Link></Card.Body>
                    </Card>
                </ListGroup.Item>
                <ListGroup.Item>
                    <Card className="text-center">
                        <Card.Body className="custom-card-body"><Link to="/chat-rooms">My chat rooms</Link></Card.Body>
                    </Card>
                </ListGroup.Item>
                <ListGroup.Item>
                    <Card className="text-center">
                        <Card.Body className="custom-card-body"><Link to="/chat-invitations">My
                            invitations</Link></Card.Body>
                    </Card>
                </ListGroup.Item>
            </ListGroup>
            {myChannels.map((channel, index) => (
                <ListGroup.Item key={index}>
                    <Card className="text-center mb-2 shadow-sm">
                        <Card.Body className="custom-card-body">
                            <Link to={`chat-window/${channel.id}/${channel.title}`}>#{channel.title}</Link>
                        </Card.Body>
                    </Card>
                </ListGroup.Item>
            ))}
            {myInvitedChannels.map((channel, index) => (
                <ListGroup.Item key={index}>
                    <Card className="text-center mb-2 shadow-sm">
                        <Card.Body className="custom-card-body">
                            <Link to={`chat-window/${channel.id}/${channel.title}`}>##{channel.title}</Link>
                        </Card.Body>
                    </Card>
                </ListGroup.Item>
            ))}
        </div>
    );
}

export default ChannelList;
