import { useState, useEffect } from 'react';
import UserSession from "../UserSession";

function useGetChatrooms() {
    const [chatrooms, setChatrooms] = useState([]);

    useEffect(() => {
        let myChannels = [];
        fetch('/api/users/' + UserSession.getUserId() + '/owner_channels?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('hiiiiiiii');
                }
                return response.json();
            })
            .then(data => {
                //dispatch(allMyChannels(data));
                const channelIds = data.map(channel => channel.id);
                console.log("My channel ids:");
                console.log(channelIds);
                myChannels = channelIds;
                setChatrooms(channelIds);
            })
            .catch(error => {
                console.error('Error:', error);
            });

        fetch('/api/users/' + UserSession.getUserId() + '/invite_channels?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
                const chanIds = data.map(channel => channel.id);
                console.log("Invite channel ids:");
                console.log(chanIds);
                myChannels = myChannels.concat(chanIds);
                setChatrooms(myChannels);
                console.log('Success invite channels:', chatrooms);
            })
            .catch(error => {
                console.error('Error:', error);
            });

    }, []);

    return { chatrooms };
}

export default useGetChatrooms;