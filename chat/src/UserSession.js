class UserSession {
    static setToken(token) {
        sessionStorage.setItem('token', token);
    }

    static getToken() {
        return sessionStorage.getItem('token');
    }

    static setUserId(userId) {
        sessionStorage.setItem('id', userId);
    }

    static getUserId() {
        return sessionStorage.getItem('id');
    }

    static setUsername(username) {
        sessionStorage.setItem('username', username);
    }

    static getUsername() {
        return sessionStorage.getItem('username');
    }

    static clearSession() {
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('id');
        sessionStorage.removeItem('username');
    }
}

export default UserSession;
