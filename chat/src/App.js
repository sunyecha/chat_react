import React, {useEffect, useState} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import ChannelList from './appPages/ChannelList';
import ChatWindow from './appPages/ChatWindow';
import WelcomeMessage from './components/WelcomeMessage';
import CreateChannel from './appPages/CreateChannel';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import MyChatRooms from './components/MyChatRooms';
import MyInvitations from './components/MyInvitations';

import { useDispatch , useSelector } from 'react-redux';
import { allUsers } from './redux/reducer/allUsersSlice';
import { allMyChannels } from './redux/reducer/allMyChannelsSlice';
import { allInvitedChannels } from './redux/reducer/allInvitedChannelsSlice';
import UserSession from "./UserSession";
import StompHandler from "./websocket/StompHandler";

function App() {
    const [myChannels, setMyChannels] = useState([]);
    const [inviteChannels, setInviteChannels] = useState([]);

    // get userId from query params
    useEffect(() => {
        const queryParams = new URLSearchParams(window.location.search);
        const id = queryParams.get('id');
        const username = queryParams.get('username');
        const token = queryParams.get('token')

        UserSession.setUserId(id);
        UserSession.setUsername(username);
        UserSession.setToken(token);
    }, []);

  // fetch all users
  const dispatch = useDispatch();
  
  useEffect(() => {
    fetch('/api/users/all?username=' + UserSession.getUsername(), {
      method: 'GET',
      headers: {
        'accept': 'application/json',
        'Authorization': 'Bearer ' + UserSession.getToken(),
      }
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      } else {
        console.log('ok');
      }
      return response.json();
    })
    .then(data => {
      dispatch(allUsers(data));
    })
    .catch(error => {
      console.error('Error:', error);
    });
  }, []);
    // fetch all users
    const [messages, setMessages] = useState({});

    useEffect(() => {
        fetch('/api/users/all?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                // 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
                dispatch(allUsers(data));
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, []);

    // fetch all my channels
    const newRoom = useSelector(state => state.checkNewRoom.value);

    useEffect(() => {
        let myChannels = [];
        fetch('/api/users/' + UserSession.getUserId() + '/owner_channels?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
                dispatch(allMyChannels(data));
                const channelIds = data.map(channel => channel.id);
                setMyChannels(channelIds);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, [newRoom]);

    // fetch all channels invited
    useEffect(() => {
        fetch('/api/users/' + UserSession.getUserId() + '/invite_channels?username=' + UserSession.getUsername(), {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                'Authorization': 'Bearer ' + UserSession.getToken(),
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                } else {
                    console.log('ok');
                }
                return response.json();
            })
            .then(data => {
                dispatch(allInvitedChannels(data));
                const channelIds = data.map(channel => channel.id);
                setInviteChannels(channelIds);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, []);


  return (
      <StompHandler serverUrl={"http://localhost:8080/ws"}>
          <Router>
              <Container fluid className="container-fluid">
                  <Row className="chat-container">
                      <Col className="sidebar">
                          <ChannelList />
                      </Col>
                      <Col className="chat-window">
                          <Routes>
                              <Route path="chat-window/:channelId/:channelName" element={<ChatWindow />} />
                              <Route path="/chat-rooms" element={<MyChatRooms />} />
                              <Route path="/chat-invitations" element={<MyInvitations />} />
                              <Route path="/create-chat" element={<CreateChannel />} />
                              <Route path="*" element={<WelcomeMessage />} />
                          </Routes>
                      </Col>
                  </Row>
              </Container>
          </Router>
      </StompHandler>
  );
}

export default App;
