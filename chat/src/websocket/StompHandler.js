import React, { createContext, useContext, useEffect, useState  } from 'react';
import { Client } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import useGetChatrooms from "../hooks/useGetChatrooms";
import UserSession from "../UserSession";
import { useSelector } from 'react-redux';
import ChatWindow from '../appPages/ChatWindow';

// Create a Context for the chat
const StompContext = createContext();


// Custom hook to use the Chat context
export const useStomp = () => {
    return useContext(StompContext);
};

const StompHandler = ({ serverUrl, children }) => {
    const [stompClient, setStompClient] = useState(null);
    const [connected, setConnected] = useState(false);
    const [messages, setMessages] = useState({});
    const [subscribedRooms, setSubscribedRooms] = useState([]);
    // const { chatrooms } = useGetChatrooms();

    const allMyChannels = useSelector(state => state.allMyChannels.list);
    const allInvitedChannels = useSelector(state => state.allInvitedChannels.list);

    const allChannels = allMyChannels.concat(allInvitedChannels);

    const chatrooms = allChannels.map(channel => channel.id);
    // const chatrooms = allMyChannels.concat(allInvitedChannels);
    useEffect(() => {
        setSubscribedRooms(chatrooms);
        if (stompClient && stompClient.connected) {
            return;
        }

        const client = new Client({
            brokerURL: serverUrl,
            webSocketFactory: () => new SockJS(serverUrl),
            onConnect: () => {
                setConnected(true);
                console.log("channels to subscribe to: ", chatrooms);
                // console.log("channels to subscribe to: ", allMyChannels);
                if (!chatrooms) {
                    return;
                }
                chatrooms.forEach(room => {
                    console.log("Subscribing to chatroom: ", room);
                    client.subscribe(`/channel/${room}`, message => {
                        console.log("Received message :", message);
                        const parsedMessage = JSON.parse(message.body);
                        setMessages(prevMessages => ({
                            ...prevMessages,
                            [room]: [...(prevMessages[room] || []), parsedMessage]
                        }));
                    });
                });
            },
            onWebSocketError: (error) => {
                console.error('Error with websocket', error);
            },
            onStompError: (frame) => {
                console.error('Broker reported error: ' + frame.headers['message']);
                console.error('Additional details: ' + frame.body);
            },
            reconnectDelay: 5000,
            heartbeatIncoming: 4000,
            heartbeatOutgoing: 4000,
        });

        client.activate();
        console.log("Stomp client activated");
        setStompClient(client);

        return () => {
            if (client.connected) {
                client.deactivate();
            }
        };
    }, [serverUrl]);

    useEffect(() => {
        const newRooms = chatrooms.filter(room => !subscribedRooms.includes(room));
        if (newRooms.length > 0) {
        newRooms.forEach(room => {
            console.log("Subscribing to chatroom: ", room);
            stompClient.subscribe(`/channel/${room}`, message => {
                console.log("Received message :", message);
                const parsedMessage = JSON.parse(message.body);
                setMessages(prevMessages => ({
                    ...prevMessages,
                    [room]: [...(prevMessages[room] || []), parsedMessage]
                }));
            });
        });
        setSubscribedRooms(currentRooms => [...currentRooms, ...newRooms]);
        }
    },[chatrooms, stompClient, subscribedRooms]);

    const sendMessage = (roomId, message) => {
        if (stompClient && stompClient.connected) {
            const chatMessage = {
                sender: UserSession.getUserId(),
                content: message,
                type: 'CHAT'
            };
            stompClient.publish({
                destination: `/app/chat/${roomId}/sendMessage`,
                body: JSON.stringify(chatMessage)
            });
        } else {
            console.error('WebSocket client is not connected.');
        }
    };

    return (
        <StompContext.Provider value={{ connected, messages, sendMessage }}>
            {children}
        </StompContext.Provider>
    );
};

export default StompHandler;
