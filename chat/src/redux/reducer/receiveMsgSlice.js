import { createSlice } from '@reduxjs/toolkit';

export const receiveMsgSlice = createSlice({
  name: 'receiveMsgs',
  initialState: {
    dic : {}
  },
  reducers: {
    addReceiveMsgs: (state, action) => {
      const { chatId, message } = action.payload;
    if (!state.dic[chatId]) {
        state.dic[chatId] = [];
      }
    if (message !== '') {
        state.dic[chatId].push(message);
      }
    }
  }
});

export const { addReceiveMsgs } = receiveMsgSlice.actions;
export default receiveMsgSlice.reducer;
