import { createSlice } from '@reduxjs/toolkit';

export const checkNewUserAddedSlice = createSlice({
  name: 'checkNewUserAdded',
  initialState: {
    value: 0,
  },
  reducers: {
    incrementNum: state => {
      state.value += 1;
    },
  },
});


export const { incrementNum } = checkNewUserAddedSlice.actions;

export default checkNewUserAddedSlice.reducer;
