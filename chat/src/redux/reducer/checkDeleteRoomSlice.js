import { createSlice } from '@reduxjs/toolkit';

export const checkDeleteRoomSlice = createSlice({
  name: 'checkDeleteRoom',
  initialState: {
    value: 0,
  },
  reducers: {
    increment2: state => {
      state.value += 1;
    },
  },
});


export const { increment2 } = checkDeleteRoomSlice.actions;

export default checkDeleteRoomSlice.reducer;
