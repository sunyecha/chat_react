import { createSlice } from '@reduxjs/toolkit';

export const allUsersSlice = createSlice({
  name: 'allUsers',
  initialState: {
    list: []
  },
  reducers: {
    allUsers: (state, action) => {
    state.list = action.payload;
    }
  }
});

export const { allUsers } = allUsersSlice.actions;

export default allUsersSlice.reducer;
