import { createSlice } from '@reduxjs/toolkit';

export const allInvitedChannelsSlice = createSlice({
  name: 'allInvitedChannels',
  initialState: {
    list: []
  },
  reducers: {
    allInvitedChannels: (state, action) => {
    state.list = action.payload;
    }
  }
});

export const { allInvitedChannels } = allInvitedChannelsSlice.actions;

export default allInvitedChannelsSlice.reducer;
