import { createSlice } from '@reduxjs/toolkit';

export const allMyChannelsSlice = createSlice({
  name: 'allMyChannels',
  initialState: {
    list: []
  },
  reducers: {
    allMyChannels: (state, action) => {
    state.list = action.payload;
    }
  }
});

export const { allMyChannels } = allMyChannelsSlice.actions;

export default allMyChannelsSlice.reducer;
