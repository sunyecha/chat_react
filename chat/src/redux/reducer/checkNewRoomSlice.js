import { createSlice } from '@reduxjs/toolkit';

export const checkNewRoomSlice = createSlice({
  name: 'checkNewRoom',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: state => {
      state.value += 1;
    },
  },
});


export const { increment } = checkNewRoomSlice.actions;

export default checkNewRoomSlice.reducer;
