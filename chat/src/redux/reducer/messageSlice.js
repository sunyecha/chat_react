import { createSlice } from '@reduxjs/toolkit';

export const messageSlice = createSlice({
  name: 'messages',
  initialState: {
    dic : {}
  },
  reducers: {
    addMessage: (state, action) => {
      const { chatId, message } = action.payload;
    if (!state.dic[chatId]) {
        state.dic[chatId] = [];
      }
    if (message.content !== '') {
        state.dic[chatId].push(message);
      }
    }
  }
});

export const { addMessage } = messageSlice.actions;
export default messageSlice.reducer;
