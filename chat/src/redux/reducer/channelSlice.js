import { createSlice } from '@reduxjs/toolkit';

export const channelSlice = createSlice({
  name: 'channels',
  initialState: {
    list: []
  },
  reducers: {
    addChannel: (state, action) => {
      state.list.push(action.payload);
    }
  }
});

export const { addChannel } = channelSlice.actions;

export default channelSlice.reducer;
