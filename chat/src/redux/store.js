import { configureStore } from '@reduxjs/toolkit';
import channelReducer from './reducer/channelSlice';
import messageReducer from './reducer/messageSlice';
import allUsersReducer from './reducer/allUsersSlice';
import receiveMsgReducer from './reducer/receiveMsgSlice';
import checkNewRoomReducer from './reducer/checkNewRoomSlice'
import allMyChannelsReducer from './reducer/allMyChannelsSlice'
import allInvitedChannelsReducer from './reducer/allInvitedChannelsSlice'
import checkNewUserAddedReducer from './reducer/checkNewUserAddedSlice'
import checkDeleteRoomReducer from './reducer/checkDeleteRoomSlice';


export const store = configureStore({
  reducer: {
    channels: channelReducer,
    messages: messageReducer,
    allUsers: allUsersReducer,
    receiveMsgs: receiveMsgReducer,
    checkNewRoom: checkNewRoomReducer,
    allMyChannels: allMyChannelsReducer,
    allInvitedChannels: allInvitedChannelsReducer,
    checkNewUserAdded: checkNewUserAddedReducer,
    checkDeleteRoom: checkDeleteRoomReducer,
  }
});
