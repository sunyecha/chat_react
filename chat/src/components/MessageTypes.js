import React from 'react';
import {Box, Divider, Stack, Typography} from "@mui/material";
import UserSession from "../UserSession";
import {useTheme} from "@mui/material/styles";

const TextMessage = ({message}) => {
    const theme = useTheme();

    return (
      <Stack direction="row" justifyContent={message.sender === UserSession.getUsername() ? "end" : "start"}>
          <Box p={2} sx={{ backgroundColor: message.sender === UserSession.getUsername() ? theme.palette.primary.main : theme.palette.background.default, borderRadius: 1.5, width: "max-content" }}>
              <Typography variant="body2" color={message.sender === UserSession.getUsername() ? "#fff" : theme.palette.text}>
                  {message.content}
              </Typography>
          </Box>
      </Stack>
    );
};

const JoinMessage = ({ message }) => {
    const theme = useTheme();

    return (
        <Stack direction="row" alignItems={"center"} justifyContent="space-between">
            <Divider width="25%" />
            <Typography variant="caption" sx={{ color: theme.palette.text }}>
                {message.sender} has joined the conversation
            </Typography>
            <Divider width="25%" />
        </Stack>
    );
};

const LeaveMessage = ({ message }) => {
    const theme = useTheme();

    return (
        <Stack direction="row" alignItems={"center"} justifyContent="space-between">
            <Divider width="25%" />
            <Typography variant="caption" sx={{ color: theme.palette.text }}>
                {message.sender} has left the conversation
            </Typography>
            <Divider width="25%" />
        </Stack>
    );
};

export {TextMessage, JoinMessage, LeaveMessage};