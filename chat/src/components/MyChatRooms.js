import React, { useState , useEffect , useMemo } from 'react';
import { useSelector } from 'react-redux';
// import { addMessage, selectMessages } from './features/messages/messageSlice';
import UserSession from "../UserSession";
import './css/ChatApp.css';

function MyChatRooms() {
  const userID = UserSession.getUserId();

  //If a new channel is created, the list of channels is refreshed again using useEffect
  const newRoom = useSelector(state => state.checkNewRoom.value);

  // console.log(newRoom);
  const [channelsProp, setChannelsProp] = useState([]);

  //pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(2);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  


  //get all rooms
  useEffect(() => {
    fetch('/api/users/' + userID + '/owner_channels?username=' + UserSession.getUsername(), {
      method: 'GET',
      headers: {
        'accept': 'application/json',
        'Authorization': 'Bearer ' + UserSession.getToken(),
      }
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      } else {
        console.log('ok');
      }
      return response.json();
    })
    .then(data => {
      setChannelsProp(data);
      console.log('Success:', data);
    })
    .catch(error => {
      console.error('Error:', error);
    });
  }, [newRoom]);

  //sort
  const sortedChannels = useMemo(() => {
    return channelsProp.sort((a, b) => a.id - b.id);
  }, [channelsProp]);
  console.log(sortedChannels);

  const currentItems = sortedChannels.slice(indexOfFirstItem, indexOfLastItem);

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(sortedChannels.length / itemsPerPage); i++) {
      pageNumbers.push(i);
  }



  return (
    <div style={{  
      height: '100vh',       
    }}>
    <h1 className="table-title">My Chats</h1>
    <table>
      <thead>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Date</th>
          <th>Duration (min)</th>
        </tr>
      </thead>
      <tbody>
        {currentItems.map(channel => (
          <tr key={channel.id}>
            <td>{channel.title}</td>
            <td>{channel.description}</td>
            <td>{channel.date}</td>
            <td>{channel.duration}</td>
          </tr>
        ))}
      </tbody>
    </table>
    <div style={{ display: 'flex', justifyContent: 'center' }}>
        {pageNumbers.map(number => (
            <button key={number} onClick={() => paginate(number)}>
                {number}
            </button>
        ))}
    </div>
  </div>
  );
    
}

export default MyChatRooms;