import React, { useState , useEffect , useMemo } from 'react';
import UserSession from "../UserSession";
import './css/ChatApp.css';

function MyInvitations() {
  const userID = UserSession.getUserId();
  const [channelsProp, setChannelsProp] = useState([]);

  // get all invited rooms
  useEffect(() => {
    fetch('/api/users/' + userID + '/invite_channels?username=' + UserSession.getUsername(), {
      method: 'GET',
      headers: {
        'accept': 'application/json',
        'Authorization': 'Bearer ' + UserSession.getToken(),
      }
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      } else {
        console.log('ok');
      }
      return response.json();
    })
    .then(data => {
      setChannelsProp(data);
      console.log('Success:', data);
    })
    .catch(error => {
      console.error('Error:', error);
    });
  }, []);

  //pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(1);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;

  //sort
  const sortedChannels = useMemo(() => {
    return channelsProp.sort((a, b) => a.id - b.id);
  }, [channelsProp]);

  const currentItems = sortedChannels.slice(indexOfFirstItem, indexOfLastItem);

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(sortedChannels.length / itemsPerPage); i++) {
      pageNumbers.push(i);
  }


  return (
    <div style={{
      height: '100vh',                
    }}>
    <h1 className="table-title">My Invitations</h1>
    <table>
      <thead>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Date</th>
          <th>Duration (min)</th>
        </tr>
      </thead>
      <tbody>
        {currentItems.map(channel => (
          <tr key={channel.id}>
            <td>{channel.title}</td>
            <td>{channel.description}</td>
            <td>{channel.date}</td>
            <td>{channel.duration}</td>
          </tr>
        ))}
      </tbody>
    </table>
    <div>
        {pageNumbers.map(number => (
            <button key={number} onClick={() => paginate(number)}>
                {number}
            </button>
        ))}
    </div>
  </div>
  );
    
}

export default MyInvitations;