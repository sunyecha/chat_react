import React from 'react';

function WelcomeMessage() {
  return (
    <div style={{
      display: 'flex',          
      justifyContent: 'center',
      alignItems: 'center',    
      height: '100vh',         
      fontSize: '24px',        
      fontWeight: 'bold'       
    }}>
      Welcome to Chat App!
    </div>
  );
}

export default WelcomeMessage;
