import React, { useState,useEffect } from 'react';
import { useSelector } from 'react-redux';
import './css/ChannelHeader.css';
import image from '../img/personal-portrait.jpg';
import UserSession from "../UserSession";

function ChannelHeader() {
    const allUsers = useSelector(state => state.allUsers.list);
    const id = UserSession.getUserId();
    console.log('id:', id);
    // firstname
    const [firstName, setFirstName] = useState('');

    // // lastname
    const [lastName, setLastName] = useState(''); 

    useEffect(() => {
        const item = allUsers.find(item => item.id === id);
        console.log('Item:', item);
        if (item) {
            console.log('Item:', item);
            setFirstName(item.firstName);
            setLastName(item.lastName);
        }
    }, [allUsers]);

    // logout
    function handleUserAction() {
        UserSession.clearSession();
        window.location.href = 'http://localhost:8080/login';
    }

    return (
        <div className="channel-header d-flex justify-content-between align-items-center" style={{ position: 'relative'}}>
            <div>
                <img src={image} alt="Home" style={{ width: '40px', height: '40px' }} />
                <span style={{
                    position: 'absolute',
                    left: '50%',
                    top: '50%',
                    transform: 'translate(-50%, -50%)',
                    color: 'black',
                    fontSize: '20px',
                    textShadow: '1px 1px 2px black'}}>
                    Hi! {firstName} {lastName}
                </span>
            </div>
            <button style={{
                backgroundColor: 'red', 
                borderRadius: '8px',    
                color: 'white',         
                padding: '5px 10px',    
                border: 'none'  ,
                marginRight: '10px',     
                }}
                onClick={() => handleUserAction()}
                >
                Logout
            </button>
        </div>
    );
}
export default ChannelHeader;
